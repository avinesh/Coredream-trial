@extends('site::layout')

@section('content')
    <div class="container" style="padding: 50px 0px;">
        <div class="row">
            <div class="col-lg-12">
                <h1>Online Bio Data </h1>
                <p class="text-info">Please fill up the form below</p>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-6">
                @include('flash::message')
                <form action="{{route('biodata.store')}}" method="post">
                    {!! Form::open(['route'=>'biodata.store','method'=>'POST','role'=>'form']) !!}
                    <fieldset>

                        <div class="form-group">
                            <label for="name">Name</label>
                            {!! Form::text('name', $value = null, ['id'=>'name','placeholder'=>'Enter Your Name','class'=>'form-control']) !!}

                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>

                        <div class="form-check">
                            <label for="gender">Choose Your Gender</label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                {!!  Form::radio('gender', 'M', true); !!}

                                Male
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                {!!  Form::radio('gender', 'F', true); !!}

                                Female
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                {!!  Form::radio('gender', 'other', true); !!}
                                Other
                            </label>
                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                        </div>


                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            {!! Form::text('phone', $value = null, ['id'=>'phone','placeholder'=>'Enter Phone Number','class'=>'form-control']) !!}
                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            {!! Form::text('email', $value = null, ['id'=>'email','placeholder'=>'Enter Your email','class'=>'form-control']) !!}
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.
                            </small>
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            {!! Form::text('address', $value = null, ['id'=>'address','placeholder'=>'Enter Your Address','class'=>'form-control']) !!}

                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        </div>


                        <div class="form-group">
                            <label for="nationality">Nationality</label>
                            {!! Form::text('nationality', $value = null, ['id'=>'nationality','placeholder'=>'Enter Your Nationality','class'=>'form-control']) !!}

                            <span class="text-danger">{{ $errors->first('nationality') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="dateOfBirth">Date Of Birth</label>
                            {!! Form::text('dateOfBirth', $value = null, ['id'=>'dateOfBirth','placeholder'=>'Eg.2074/05/06','class'=>'form-control']) !!}

                            <span class="text-danger">{{ $errors->first('dateOfBirth') }}</span>
                        </div>


                        <div class="form-group">
                            <label for="education">Education</label>
                            {!! Form::text('education', $value = null, ['id'=>'education','placeholder'=>'Enter Your Educatio','class'=>'form-control']) !!}

                            <span class="text-danger">{{ $errors->first('education') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="exampleSelect1">Preffered Mode Of Contact</label>
                            {!! Form::select('prefered_contact', ['phone' => 'Phone', 'email' => 'Email','none'=>'None']); !!}

                            <span class="text-danger">{{ $errors->first('prefered_contact') }}</span>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </fieldset>
                    {!! Form::close() !!}
                </form>
            </div>
        </div>
    </div>
@endsection