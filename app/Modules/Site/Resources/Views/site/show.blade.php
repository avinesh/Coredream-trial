@extends('site::layout')

@section('content')
    <div class="container" style="padding: 50px 0px;">
        <div class="row">
            <div class="col-lg-12">
                <h1>Data You Have Entered </h1>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">
                <table class="table table-hover">
                    <thead>
                    <tr>

                        <th scope="col">Row No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Nationality</th>
                        <th scope="col">Date Of Birth</th>
                        <th scope="col">Education</th>
                        <th scope="col">Prefered Contact</th>


                    </tr>
                    </thead>
                    <tbody>

                    <tr class="table-active">
                        <th>{{$latestRow[0]}}</th>
                        <td>{{$latestRow[1]}}</td>
                        <td>{{$latestRow[2]}}</td>
                        <td>{{$latestRow[3]}}</td>
                        <td>{{$latestRow[4]}}</td>
                        <td>{{$latestRow[5]}}</td>
                        <td>{{$latestRow[6]}}</td>
                        <td>{{$latestRow[7]}}</td>
                        <td>{{$latestRow[8]}}</td>
                        <td>{{$latestRow[9]}}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection