<?php

namespace App\Modules\Biodata\Providers;

use App\Modules\BioData\Repositories\BioDataInterface;
use App\Modules\BioData\Repositories\BioDataRepository;
use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Resources/Lang', 'biodata');
        $this->loadViewsFrom(__DIR__.'/../Resources/Views', 'biodata');
        $this->loadMigrationsFrom(__DIR__.'/../Database/Migrations', 'biodata');
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->BioDataRegister();
    }

    public function BioDataRegister()
    {
        $this->app->bind(

            BioDataInterface::class,
            BioDataRepository::class
        );
    }
}
