<?php
namespace App\Modules\BioData\Repositories;


interface BioDataInterface
{
    public function writeCsvFile($data);

    public function readLatestDataInCsv();

    public function cleanString($value);



}
