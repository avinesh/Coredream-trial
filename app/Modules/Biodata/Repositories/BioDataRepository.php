<?php
namespace App\Modules\BioData\Repositories;



class BioDataRepository implements BioDataInterface
{


    public function writeCsvFile($data){
        $fileOpen = fopen(base_path('storage/exports/').'dataBio.csv','a');
        $noOfRow = count(file(base_path('storage/exports/').'dataBio.csv'));


        $address=explode(',',$data['address']);
        $data['address']=implode('-',$address);


        $formData=array(
            'sn'=>$noOfRow,
            'name'=>$data['name'],
            'gender'=>$data['gender'],
            'phone'=>$data['phone'],
            'email'=>$data['email'],
            'address'=>$data['address'],
            'nationality'=>$data['nationality'],
            'dateOfBirth'=>$data['dateOfBirth'],
            'education'=>$data['education'],
            'prefered_contact'=>$data['prefered_contact']

        );
        $result=fputcsv($fileOpen,$formData);
        return $result;
    }

    public function readLatestDataInCsv()
    {
        $fileOpen = file(base_path('storage/exports/').'dataBio.csv');

        foreach ($fileOpen as $key=>$row){
            $csv[]=explode(',',$row);
            $key=$key;
        }
        return $csv[$key];
    }



    public function cleanString($string)
    {
        $string =trim($string);
        $string=stripcslashes($string);
        $string=htmlspecialchars($string);

        return $string;

    }
}
