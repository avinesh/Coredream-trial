<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



Route::group(['prefix' => 'admin/biodata'], function () {


    Route::post('store', ['as' => 'biodata.store', 'uses' => 'BioDataController@store']);

    Route::get('show', ['as' => 'biodata.show', 'uses' => 'BioDataController@show']);

});