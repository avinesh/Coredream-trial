<?php

namespace App\Modules\Biodata\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BioDataFormRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
                return [
                    'name' => 'required|min:3',
                    'gender'=>'required',
                    'phone' => 'required|numeric',
                    'email' => 'required',
                    'address'=>'required',
                    'dateOfBirth'=>'required',
                    'nationality'=>'required',
                    'education'=>'required',
                    'prefered_contact'=>'required',



                ];
            case 'PUT':
                return [
                    'name' => 'required|min:3',
                    'gender'=>'required',
                    'phone' => 'required|numeric',
                    'email' => 'required',
                    'address'=>'required',
                    'dateOfBirth'=>'required',
                    'nationality'=>'required',
                    'education'=>'required',
                    'prefered_contact'=>'required',
                ];
            default:
                break;
        }
    }


}
