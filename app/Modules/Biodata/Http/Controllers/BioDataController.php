<?php

namespace App\Modules\Biodata\Http\Controllers;

use App\Modules\Biodata\Http\Requests\BioDataFormRequest;
use App\Modules\BioData\Repositories\BioDataInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use Laracasts\Flash\Flash;


class BioDataController extends Controller
{
    protected  $bioData;

    public function __construct(BioDataInterface $bioData)
    {
        $this->bioData= $bioData;
    }

    public function create(Request $request){


    }

    public function store(BioDataFormRequest $request){

        $data=$request->all();

        try{

            $this->bioData->writeCsvFile($data);

            Flash::success('Data Sucessfully Inserted');


        }catch (\Throwable $t) {

            Flash::error( $t->getMessage());
        }

        return redirect('admin/biodata/show');


    }

    public function show(){
        try{

            $latestRow=$this->bioData->readLatestDataInCsv();

            $address = explode("-",$latestRow[5]);

            $latestRow[5]=implode(',',$address);

            dd($latestRow);


            return view('site::site.show',compact('latestRow'));


        }catch (\Throwable $t) {

            Flash::error( $t->getMessage());
        }


    }
}
